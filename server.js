'use strict';

const request = require("request");
const express = require('express');
const path = require('path');
// Constants
const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || 'localhost';
const mongoose = require('mongoose');

const fs = require('fs');


// App
const app = express();

app.set('view engine', 'ejs');

app.set('views', path.join(__dirname, 'src/views'));


app.use(express.urlencoded({ extended: false }));


// Connect to MongoDB
mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    {  promiseLibrary: global.Promise,
      useNewUrlParser: true,
      useUnifiedTopology: true}
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));
  mongoose.set('strictQuery', true);

const Item = require('./src/models/items');
const { json } = require("express/lib/response");

app.get('/', (req, res) => {
  Item.find()
    .then(items => res.render('index', { items }))
    .catch(err => res.status(404).json({ msg: 'No items found' }));
});

app.post('/item/add', (req, res) => {
  const newItem = new Item({
    name: req.body.name
  });

  newItem.save().then(item => res.redirect('/'));
});


// app.get('/', (req, res) => {
//   return res.json(`Hello World  ff 4:`+PORT);
// });

app.get('/web', function(req,res) {
  res.render('index');
});

app.get('/api', function(req,res) {
  fs.writeFileSync(`./src/file/${process.env.PORT}-${Date.now()}.json`, JSON.stringify({test:'test volume'}));
  res.json({
    test: process.env.TEST_VALUE,
    test2: process.env.NODE_ENV
  })
});



app.listen(PORT);
console.log(`Running on port :${PORT}`);
